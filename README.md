# go-build-container
[![license](https://img.shields.io/badge/License-MIT-brightgreen.svg)](https://img.shields.io/badge/License-MIT-brightgreen.svg)

A simple Docker container for building Go applications with GitLab CI.
It includes golint and LLVM in order to provide Go linting and memory sanitation testing out of the box.

The Dockerfile is based on an article by Julien Andrieux: https://about.gitlab.com/2017/11/27/go-tools-and-gitlab-how-to-do-continuous-integration-like-a-boss/

To build locally and deploy the container into the GitLab registry type:
```
docker login registry.gitlab.com
docker build -t registry.gitlab.com/<your name>/go-build-container .
docker push registry.gitlab.com/<your name>/go-build-container
```

# Docker on macOS
For building containers locally (and deploy to GitLab repository), I had to install Docker on my Mac.
I am using Homebrew:
`brew cask install docker`

Then, in the programs folder, double-click on the Docker icon.
This will set the path for command line usage and will also start the Docker daemon.

When prompted for credentials on `docker login ...` I had to use my GitLab credentials, not the passphrase for the SSH key.

